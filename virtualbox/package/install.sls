# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as virtualbox with context %}

virtualbox-package-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ virtualbox.pkgs }}
