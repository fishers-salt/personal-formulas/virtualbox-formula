# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as virtualbox with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('virtualbox-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_virtualbox', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

virtualbox-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

virtualbox-config-file-machine-folder-{{ name }}-created:
  file.directory:
    - name: {{ home }}/.local/share/virtualbox_vms
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - virtualbox-config-file-user-{{ name }}-present

virtualbox-config-file-machine-folder-{{ name }}-set:
  cmd.run:
    - name: vboxmanage setproperty machinefolder {{ home }}/.local/share/virtualbox_vms
    - runas: {{ name }}
    - require:
      - virtualbox-config-file-machine-folder-{{ name }}-created
      - sls: {{ sls_package_install }}
    - unless: grep SystemProperties {{ home }}/.config/VirtualBox/VirtualBox.xml | grep defaultMachineFolder | grep {{ home }}/.local/share/virtualbox_vms

{% endif %}
{% endfor %}
{% endif %}
