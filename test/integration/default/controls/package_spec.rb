# frozen_string_literal: true

packages = %w[virtualbox virtualbox-guest-iso virtualbox-host-modules-arch]

packages.each do |pkg|
  control "virtualbox-package-install-#{pkg}-installed" do
    title 'it should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
