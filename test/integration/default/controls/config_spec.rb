# frozen_string_literal: true

control 'virtualbox-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'virtualbox-config-file-dunst-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.local/share/virtualbox_vms') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'virtualbox-config-file-machine-folder-set' do
  title 'it should be set correctly'

  describe xml('/home/auser/.config/VirtualBox/VirtualBox.xml') do
    its('VirtualBox/Global/SystemProperties/attribute::defaultMachineFolder') do
      should eq ['/home/auser/.local/share/virtualbox_vms']
    end
  end
end
