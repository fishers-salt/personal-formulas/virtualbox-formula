# frozen_string_literal: true

packages = %w[virtualbox virtualbox-guest-iso virtualbox-host-modules-arch]

packages.each do |pkg|
  control "virtualbox-package-clean-#{pkg}-removed" do
    title 'it should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
