# frozen_string_literal: true

control 'virtualbox-config-clean-dunst-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.local/share/virtualbox_vms') do
    it { should_not exist }
  end
end
